def addition(x,y):
    return x+y  

def subtraction(x,y):
    return x-y

def division(x,y):
    return x/y

def multiplication(x,y):
    return x*y

def exponent(x,y):
    return x**y

operation = input("which operation (addition/substraction/multiplication/division/exponent): ") 
value1 = int(input("first number: ") )
value2 = int(input("second number: ") )

if operation=='addition':
    print(f'{value1} + {value2} = {addition(value1,value2)}')
elif operation=='subtraction':
    print(f'{value1} - {value2} = {subtraction(value1,value2)}')
elif operation=='multiplication':
    print(f'{value1} x {value2} = {multiplication(value1,value2)}')
elif operation=='division':
    print(f'{value1} / {value2} = {division(value1,value2)}')
elif operation=='exponent':
    print(f'{value1} ** {value2} = {exponent(value1,value2)}')
else:
    raise Exception('operation not allowed')

