# README
This is for homework week08 assignment 3.

### Change log

* 0.1.0 initial commit.

* 0.2.0 created main script: create a main script: ask for the operation wanted and two numbers for the operation.

* 0.2.1 fix a typo.

* 0.3.0 feature: addition function, create an addition function: use the two numbers and print the operation and the result.

* 0.4.0 reorganize the files from seperate functions to simple script with everything defined in a single script.

* 0.5.0 feature: subtraction function, create a subtraction function: use the two numbers and print the operation and the result.

* 0.6.0 feature: division function, create a division function: use the two numbers and print the operation and the result.

* 0.7.0 feature: multiplication function, create a multiplication function: use the two numbers and print the operation and the result.

* 0.7.1 fix mistakes in subtraction and multiplication.

* 0.8.0 feature: exponent function, create a exponent function: use the two numbers and print the operation and the result.

* 0.8.1 reformat division and exponent functions.

* 1.0.0 release of public version.
